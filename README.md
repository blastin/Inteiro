# Inteiro 1.0
Biblioteca para números do tipo inteiro com dimensão acima de 64 bits.

## Informações
A biblioteca foi desenvolvida utilizando :

* GNU/linux Debian stretch
* GCC 6.3 
* Valgrind 3.12.0 SVN
* GDB (Debian 7.12-6)
* cmake (3.6)

## Testes Unitários

* `cmake CMakeLists.txt && make && make test `

## Bibliotecas Desenvolvidas

* [ToolsMemory](https://github.com/blastin/Inteiro/blob/master/src/main/string/memory.h)    - Manipula String
* [ToolsInteiro](https://github.com/blastin/Inteiro/blob/master/src/main/inteiro/inteiro.h) - Funções básicas do Inteiro

## Configurando CMakeLists.txt próprio

Para utilizar as bibliotecas básicas do Inteiro insira em sua CMakeLists.txt a seguinte linha de código:

``` cmake
add_subdirectory (src/main/bibiliotecas/inteiro) # caso a bibilioteca inteiro esteja nesse caminho de pastas. 
target_link_libraries (Demo LINK_PUBLIC Inteiro)

```

## Operações disponíveis:
* soma
* subtração
* multiplicação
* divisão / resto

### Limitações

* sem limitações no momento.

