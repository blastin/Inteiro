#include <assert.h>
#include "../../main/inteiro/inteiro.operacoes.basicas.h"

static void inteiro_operacao_soma_test()
{
    struct Inteiro *A;
    struct Inteiro *B;
    struct Inteiro *resultado;

    A = criarInteiroComValorString("18", positivo);
    B = criarInteiroComValorString("14", positivo);
    resultado = criarInteiroVazio();

    inteiro_operacao_soma(resultado, A, B);
    assert(strcmp(resultado->numero, "32") == 0);
    assert(resultado->signal == positivo);

    atualizarNumero(A, "14", positivo);
    atualizarNumero(B, "19", positivo);

    inteiro_operacao_soma(resultado, A, B);
    assert(strcmp(resultado->numero, "33") == 0);
    assert(resultado->signal == positivo);
    assert(A->signal == positivo);
    assert(B->signal == positivo);

    atualizarNumero(A, "14", positivo);
    atualizarNumero(B, "9", negativo);

    inteiro_operacao_soma(resultado, A, B);
    assert(strcmp(resultado->numero, "5") == 0);
    assert(resultado->signal == positivo);
    assert(A->signal == positivo);
    assert(B->signal == negativo);

    atualizarNumero(A, "13", positivo);
    atualizarNumero(B, "28", negativo);

    inteiro_operacao_soma(resultado, A, B);
    assert(strcmp(resultado->numero, "15") == 0);
    assert(resultado->signal == negativo);
    assert(A->signal == positivo);
    assert(B->signal == negativo);

    atualizarNumero(A, "37", negativo);
    atualizarNumero(B, "11", negativo);

    inteiro_operacao_soma(resultado, A, B);
    assert(strcmp(resultado->numero, "48") == 0);
    assert(resultado->signal == negativo);
    assert(A->signal == negativo);
    assert(B->signal == negativo);

    atualizarNumero(A, "47", negativo);
    atualizarNumero(B, "83", negativo);

    inteiro_operacao_soma(resultado, A, B);
    assert(strcmp(resultado->numero, "130") == 0);
    assert(resultado->signal == negativo);
    assert(A->signal == negativo);
    assert(B->signal == negativo);

    atualizarNumero(A, "79", negativo);
    atualizarNumero(B, "47", positivo);

    inteiro_operacao_soma(resultado, A, B);
    assert(strcmp(resultado->numero, "32") == 0);
    assert(resultado->signal == negativo);
    assert(A->signal == negativo);
    assert(B->signal == positivo);

    atualizarNumero(A, "14", negativo);
    atualizarNumero(B, "93", positivo);

    inteiro_operacao_soma(resultado, A, B);
    assert(strcmp(resultado->numero, "79") == 0);
    assert(resultado->signal == positivo);
    assert(A->signal == negativo);
    assert(B->signal == positivo);

    atualizarNumero(A, "27", positivo);
    atualizarNumero(B, "27", positivo);

    inteiro_operacao_soma(resultado, A, B);
    assert(strcmp(resultado->numero, "54") == 0);
    assert(resultado->signal == positivo);
    assert(A->signal == positivo);
    assert(B->signal == positivo);

    atualizarNumero(A, "39", positivo);
    atualizarNumero(B, "39", negativo);

    inteiro_operacao_soma(resultado, A, B);
    assert(strcmp(resultado->numero, "0") == 0);
    assert(resultado->signal == positivo);
    assert(A->signal == positivo);
    assert(B->signal == negativo);

    atualizarNumero(A, "71", negativo);
    atualizarNumero(B, "71", negativo);

    inteiro_operacao_soma(resultado, A, B);
    assert(strcmp(resultado->numero, "142") == 0);
    assert(resultado->signal == negativo);
    assert(A->signal == negativo);
    assert(B->signal == negativo);

    atualizarNumero(A, "49", negativo);
    atualizarNumero(B, "49", positivo);

    inteiro_operacao_soma(resultado, A, B);
    assert(strcmp(resultado->numero, "0") == 0);
    assert(resultado->signal == negativo);
    assert(A->signal == negativo);
    assert(B->signal == positivo);

    liberarMemoriaInteiro(A);
    liberarMemoriaInteiro(B);
    liberarMemoriaInteiro(resultado);
}

static void inteiro_operacao_subtracao_test()
{
    struct Inteiro *A;
    struct Inteiro *B;
    struct Inteiro *resultado;

    A = criarInteiroComValorString("18", positivo);
    B = criarInteiroComValorString("14", positivo);
    resultado = criarInteiroVazio();

    inteiro_operacao_subtracao(resultado, A, B);
    assert(strcmp(resultado->numero, "4") == 0);
    assert(resultado->signal == positivo);
    assert(A->signal == positivo);
    assert(B->signal == positivo);

    atualizarNumero(A, "36", positivo);
    atualizarNumero(B, "99", positivo);

    inteiro_operacao_subtracao(resultado, A, B);
    assert(strcmp(resultado->numero, "63") == 0);
    assert(resultado->signal == negativo);
    assert(A->signal == positivo);
    assert(B->signal == positivo);

    atualizarNumero(A, "72", positivo);
    atualizarNumero(B, "50", negativo);

    inteiro_operacao_subtracao(resultado, A, B);
    assert(strcmp(resultado->numero, "122") == 0);
    assert(resultado->signal == positivo);
    assert(A->signal == positivo);
    assert(B->signal == negativo);

    atualizarNumero(A, "13", positivo);
    atualizarNumero(B, "29", negativo);

    inteiro_operacao_subtracao(resultado, A, B);
    assert(strcmp(resultado->numero, "42") == 0);
    assert(resultado->signal == positivo);
    assert(A->signal == positivo);
    assert(B->signal == negativo);

    atualizarNumero(A, "9", negativo);
    atualizarNumero(B, "7", negativo);

    inteiro_operacao_subtracao(resultado, A, B);
    assert(strcmp(resultado->numero, "2") == 0);
    assert(resultado->signal == negativo);
    assert(A->signal == negativo);
    assert(B->signal == negativo);

    atualizarNumero(A, "11", negativo);
    atualizarNumero(B, "85", negativo);

    inteiro_operacao_subtracao(resultado, A, B);
    assert(strcmp(resultado->numero, "74") == 0);
    assert(resultado->signal == positivo);
    assert(A->signal == negativo);
    assert(B->signal == negativo);

    atualizarNumero(A, "111", negativo);
    atualizarNumero(B, "97", positivo);

    inteiro_operacao_subtracao(resultado, A, B);
    assert(strcmp(resultado->numero, "208") == 0);
    assert(resultado->signal == negativo);
    assert(A->signal == negativo);
    assert(B->signal == positivo);

    atualizarNumero(A, "19", negativo);
    atualizarNumero(B, "41", positivo);

    inteiro_operacao_subtracao(resultado, A, B);
    assert(strcmp(resultado->numero, "60") == 0);
    assert(resultado->signal == negativo);
    assert(A->signal == negativo);
    assert(B->signal == positivo);

    atualizarNumero(A, "27", positivo);
    atualizarNumero(B, "27", positivo);

    inteiro_operacao_subtracao(resultado, A, B);
    assert(strcmp(resultado->numero, "0") == 0);
    assert(resultado->signal == positivo);
    assert(A->signal == positivo);
    assert(B->signal == positivo);

    atualizarNumero(A, "39", positivo);
    atualizarNumero(B, "39", negativo);

    inteiro_operacao_subtracao(resultado, A, B);
    assert(strcmp(resultado->numero, "78") == 0);
    assert(resultado->signal == positivo);
    assert(A->signal == positivo);
    assert(B->signal == negativo);

    atualizarNumero(A, "71", negativo);
    atualizarNumero(B, "71", negativo);

    inteiro_operacao_subtracao(resultado, A, B);
    assert(strcmp(resultado->numero, "0") == 0);
    assert(resultado->signal == negativo);
    assert(A->signal == negativo);
    assert(B->signal == negativo);

    atualizarNumero(A, "49", negativo);
    atualizarNumero(B, "49", positivo);

    inteiro_operacao_subtracao(resultado, A, B);
    assert(strcmp(resultado->numero, "98") == 0);
    assert(resultado->signal == negativo);
    assert(A->signal == negativo);
    assert(B->signal == positivo);

    liberarMemoriaInteiro(A);
    liberarMemoriaInteiro(B);
    liberarMemoriaInteiro(resultado);
}

static void inteiro_operacao_multiplicao_test()
{
    struct Inteiro *A;
    struct Inteiro *B;
    struct Inteiro *resultado;

    A = criarInteiroComValorString("23", positivo);
    B = criarInteiroComValorString("69", positivo);
    resultado = criarInteiroVazio();

    inteiro_operacao_multiplicao(resultado, A, B);

    assert(strcmp(resultado->numero, "1587") == 0);
    assert(A->signal == positivo);
    assert(B->signal == positivo);
    assert(resultado->signal == positivo);

    alterarSinal(B, negativo);

    inteiro_operacao_multiplicao(resultado, A, B);

    assert(strcmp(resultado->numero, "1587") == 0);
    assert(A->signal == positivo);
    assert(B->signal == negativo);
    assert(resultado->signal == negativo);

    alterarSinal(A, negativo);

    inteiro_operacao_multiplicao(resultado, A, B);

    assert(strcmp(resultado->numero, "1587") == 0);
    assert(A->signal == negativo);
    assert(B->signal == negativo);
    assert(resultado->signal == positivo);

    alterarSinal(B, positivo);

    inteiro_operacao_multiplicao(resultado, A, B);

    assert(strcmp(resultado->numero, "1587") == 0);
    assert(A->signal == negativo);
    assert(B->signal == positivo);
    assert(resultado->signal == negativo);

    atualizarNumero(A, "0", positivo);
    atualizarNumero(B, "0", positivo);

    inteiro_operacao_multiplicao(resultado, A, B);

    assert(strcmp(resultado->numero, "0") == 0);
    assert(A->signal == positivo);
    assert(B->signal == positivo);
    assert(resultado->signal == positivo);

    atualizarNumero(A, "0", positivo);
    atualizarNumero(B, "71", positivo);

    inteiro_operacao_multiplicao(resultado, A, B);

    assert(strcmp(resultado->numero, "0") == 0);
    assert(A->signal == positivo);
    assert(B->signal == positivo);
    assert(resultado->signal == positivo);

    atualizarNumero(A, "79879", positivo);
    atualizarNumero(B, "0", positivo);

    inteiro_operacao_multiplicao(resultado, A, B);

    assert(strcmp(resultado->numero, "0") == 0);
    assert(A->signal == positivo);
    assert(B->signal == positivo);
    assert(resultado->signal == positivo);

    atualizarNumero(A, "79879", positivo);
    atualizarNumero(B, "1", positivo);

    inteiro_operacao_multiplicao(resultado, A, B);

    assert(strcmp(resultado->numero, "79879") == 0);
    assert(A->signal == positivo);
    assert(B->signal == positivo);
    assert(resultado->signal == positivo);

    atualizarNumero(A, "1", positivo);
    atualizarNumero(B, "34535645668691", positivo);

    inteiro_operacao_multiplicao(resultado, A, B);

    assert(strcmp(resultado->numero, "34535645668691") == 0);
    assert(A->signal == positivo);
    assert(B->signal == positivo);
    assert(resultado->signal == positivo);

    liberarMemoriaInteiro(A);
    liberarMemoriaInteiro(B);
    liberarMemoriaInteiro(resultado);
}

static void inteiro_operacao_divisao_test()
{

    struct Inteiro *A;
    struct Inteiro *B;
    struct Inteiro *quociente;
    struct Inteiro *resto;

    A = criarInteiroComValorString("913", positivo);
    B = criarInteiroComValorString("93", positivo);
    quociente = criarInteiroVazio();
    resto = criarInteiroVazio();

    inteiro_operacao_divisao(quociente, resto, A, B);

    assert(strcmp(quociente->numero, "9") == 0);
    assert(quociente->signal == positivo);

    assert(strcmp(resto->numero, "76") == 0);
    assert(resto->signal == positivo);

    assert(A->signal == positivo);
    assert(B->signal == positivo);

    //

    atualizarNumero(A, "11", positivo);
    atualizarNumero(B, "29", positivo);

    inteiro_operacao_divisao(quociente, resto, A, B);

    assert(strcmp(quociente->numero, "0") == 0);
    assert(quociente->signal == positivo);

    assert(strcmp(resto->numero, "11") == 0);
    assert(resto->signal == positivo);

    assert(A->signal == positivo);
    assert(B->signal == positivo);

    //

    atualizarNumero(A, "31", positivo);
    atualizarNumero(B, "14", positivo);

    inteiro_operacao_divisao(quociente, resto, A, B);

    assert(strcmp(quociente->numero, "2") == 0);
    assert(quociente->signal == positivo);

    assert(strcmp(resto->numero, "3") == 0);
    assert(resto->signal == positivo);

    assert(A->signal == positivo);
    assert(B->signal == positivo);

    //

    atualizarNumero(A, "31", negativo);
    atualizarNumero(B, "14", negativo);

    inteiro_operacao_divisao(quociente, resto, A, B);

    assert(strcmp(quociente->numero, "2") == 0);
    assert(quociente->signal == positivo);

    assert(strcmp(resto->numero, "3") == 0);
    assert(resto->signal == negativo);

    assert(A->signal == negativo);
    assert(B->signal == negativo);

    //

    atualizarNumero(A, "31", negativo);
    atualizarNumero(B, "14", positivo);

    inteiro_operacao_divisao(quociente, resto, A, B);

    assert(strcmp(quociente->numero, "3") == 0);
    assert(quociente->signal == negativo);

    assert(strcmp(resto->numero, "11") == 0);
    assert(resto->signal == positivo);
    
    assert(A->signal == negativo);
    assert(B->signal == positivo);

    //

    atualizarNumero(A, "31", positivo);
    atualizarNumero(B, "14", negativo);

    inteiro_operacao_divisao(quociente, resto, A, B);

    assert(strcmp(quociente->numero, "3") == 0);
    assert(quociente->signal == negativo);

    assert(strcmp(resto->numero, "11") == 0);
    assert(resto->signal == negativo);

    assert(A->signal == positivo);
    assert(B->signal == negativo);

    //

    atualizarNumero(A, "31", positivo);
    atualizarNumero(B, "72", negativo);

    inteiro_operacao_divisao(quociente, resto, A, B);

    assert(strcmp(quociente->numero, "1") == 0);
    assert(quociente->signal == negativo);

    assert(strcmp(resto->numero, "41") == 0);
    assert(resto->signal == negativo);

    assert(A->signal == positivo);
    assert(B->signal == negativo);

    //

    atualizarNumero(A, "31", negativo);
    atualizarNumero(B, "72", positivo);

    inteiro_operacao_divisao(quociente, resto, A, B);

    assert(strcmp(quociente->numero, "1") == 0);
    assert(quociente->signal == negativo);

    assert(strcmp(resto->numero, "41") == 0);
    assert(resto->signal == positivo);

    assert(A->signal == negativo);
    assert(B->signal == positivo);

    //

    atualizarNumero(A, "1149", negativo);
    atualizarNumero(B, "42", negativo);

    inteiro_operacao_divisao(quociente, resto, A, B);

    assert(strcmp(quociente->numero, "27") == 0);
    assert(quociente->signal == positivo);

    assert(strcmp(resto->numero, "15") == 0);
    assert(resto->signal == negativo);

    assert(A->signal == negativo);
    assert(B->signal == negativo);

    //

    atualizarNumero(A, "129", negativo);
    atualizarNumero(B, "978", negativo);

    inteiro_operacao_divisao(quociente, resto, A, B);

    assert(strcmp(quociente->numero, "0") == 0);
    assert(quociente->signal == positivo);
    
    assert(strcmp(resto->numero, "129") == 0);
    assert(resto->signal == negativo);

    assert(A->signal == negativo);
    assert(B->signal == negativo);

    //

    atualizarNumero(A, "71", negativo);
    atualizarNumero(B, "19", positivo);

    inteiro_operacao_divisao(quociente, resto, A, B);

    assert(strcmp(quociente->numero, "4") == 0);
    assert(quociente->signal == negativo);

    assert(strcmp(resto->numero, "5") == 0);
    assert(resto->signal == positivo);

    assert(A->signal == negativo);
    assert(B->signal == positivo);

    //

    atualizarNumero(A, "14", negativo);
    atualizarNumero(B, "471", positivo);

    inteiro_operacao_divisao(quociente, resto, A, B);

    assert(strcmp(quociente->numero, "1") == 0);
    assert(quociente->signal == negativo);

    assert(strcmp(resto->numero, "457") == 0);
    assert(resto->signal == positivo);
    
    assert(A->signal == negativo);
    assert(B->signal == positivo);

    liberarMemoriaInteiro(A);
    liberarMemoriaInteiro(B);
    liberarMemoriaInteiro(quociente);
    liberarMemoriaInteiro(resto);
}

int main(int argc, char *argv[])
{

    inteiro_operacao_soma_test();
    inteiro_operacao_subtracao_test();
    inteiro_operacao_multiplicao_test();
    inteiro_operacao_divisao_test();

    return 0;
}
