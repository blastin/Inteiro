#include <assert.h>
#include "../../main/inteiro/inteiro.comparacao.h"
#include "../../main/inteiro/inteiro.constructor.h"

static void comparacao_test()
{

    struct Inteiro *A;
    struct Inteiro *B;

    A = criarInteiroComValorString("123", positivo);
    B = criarInteiroComValorString("123", negativo);

    assert(primeiro_inteiro_maior(A, B));
    assert(!primeiro_inteiro_maior(B, A));
    assert(primeiro_inteiro_maior_igual(A, B));
    assert(primeiro_inteiro_menor(B, A));
    assert(primeiro_inteiro_menor_igual(B, A));
    assert(!primeiro_inteiro_igual(A, B));
    assert(primeiro_inteiro_diferente(A, B));

    atualizarNumero(A, "123", positivo);
    atualizarNumero(B, "123", positivo);

    assert(!primeiro_inteiro_maior(A, B));
    assert(!primeiro_inteiro_maior(B, A));
    assert(primeiro_inteiro_maior_igual(A, B));
    assert(primeiro_inteiro_maior_igual(B, A));
    assert(primeiro_inteiro_menor_igual(B, A));
    assert(primeiro_inteiro_menor_igual(A, B));
    assert(primeiro_inteiro_igual(A, B));
    assert(!primeiro_inteiro_diferente(A, B));

    atualizarNumero(A, "123", negativo);
    atualizarNumero(B, "123", negativo);

    assert(!primeiro_inteiro_maior(A, B));
    assert(!primeiro_inteiro_maior(B, A));
    assert(primeiro_inteiro_maior_igual(A, B));
    assert(primeiro_inteiro_maior_igual(B, A));
    assert(primeiro_inteiro_menor_igual(B, A));
    assert(primeiro_inteiro_menor_igual(A, B));
    assert(primeiro_inteiro_igual(A, B));
    assert(!primeiro_inteiro_diferente(A, B));

    atualizarNumero(A, "123", negativo);
    atualizarNumero(B, "123", positivo);

    assert(!primeiro_inteiro_maior(A, B));
    assert(primeiro_inteiro_maior(B, A));
    assert(primeiro_inteiro_maior_igual(B, A));
    assert(primeiro_inteiro_menor(A, B));
    assert(!primeiro_inteiro_igual(A, B));
    assert(primeiro_inteiro_diferente(A, B));

    //

    atualizarNumero(A, "1227474", positivo);
    atualizarNumero(B, "9958", positivo);

    assert(primeiro_inteiro_maior(A, B));
    assert(!primeiro_inteiro_maior(B, A));
    assert(primeiro_inteiro_maior_igual(A, B));
    assert(primeiro_inteiro_menor(B, A));
    assert(!primeiro_inteiro_igual(A, B));
    assert(primeiro_inteiro_diferente(A, B));

    atualizarNumero(A, "732437481", positivo);
    atualizarNumero(B, "2137132732732456813", negativo);

    assert(primeiro_inteiro_maior(A, B));
    assert(!primeiro_inteiro_maior(B, A));
    assert(primeiro_inteiro_maior_igual(A, B));
    assert(primeiro_inteiro_menor(B, A));
    assert(!primeiro_inteiro_igual(A, B));
    assert(primeiro_inteiro_diferente(A, B));

    atualizarNumero(A, "1230", negativo);
    atualizarNumero(B, "123", positivo);

    assert(!primeiro_inteiro_maior(A, B));
    assert(primeiro_inteiro_maior(B, A));
    assert(primeiro_inteiro_maior_igual(B, A));
    assert(primeiro_inteiro_menor(A, B));
    assert(!primeiro_inteiro_igual(A, B));
    assert(primeiro_inteiro_diferente(A, B));

    atualizarNumero(A, "111111", negativo);
    atualizarNumero(B, "99422", negativo);

    assert(!primeiro_inteiro_maior(A, B));
    assert(primeiro_inteiro_maior(B, A));
    assert(primeiro_inteiro_maior_igual(B, A));
    assert(primeiro_inteiro_menor(A, B));
    assert(!primeiro_inteiro_igual(A, B));
    assert(primeiro_inteiro_diferente(A, B));

    //

    atualizarNumero(A, "185", positivo);
    atualizarNumero(B, "945", positivo);

    assert(primeiro_inteiro_maior(B, A));
    assert(!primeiro_inteiro_maior(A, B));
    assert(primeiro_inteiro_maior_igual(B, A));
    assert(primeiro_inteiro_menor(A, B));
    assert(!primeiro_inteiro_igual(A, B));
    assert(primeiro_inteiro_diferente(A, B));

    atualizarNumero(A, "11", positivo);
    atualizarNumero(B, "12", negativo);

    assert(!primeiro_inteiro_maior(B, A));
    assert(primeiro_inteiro_maior(A, B));
    assert(primeiro_inteiro_maior_igual(A, B));
    assert(primeiro_inteiro_menor(B, A));
    assert(!primeiro_inteiro_igual(A, B));
    assert(primeiro_inteiro_diferente(A, B));

    atualizarNumero(A, "99999999999", negativo);
    atualizarNumero(B, "1", positivo);

    assert(primeiro_inteiro_maior(B, A));
    assert(!primeiro_inteiro_maior(A, B));
    assert(primeiro_inteiro_maior_igual(B, A));
    assert(primeiro_inteiro_menor(A, B));
    assert(!primeiro_inteiro_igual(A, B));
    assert(primeiro_inteiro_diferente(A, B));

    atualizarNumero(A, "7", negativo);
    atualizarNumero(B, "8", negativo);

    assert(!primeiro_inteiro_maior(B, A));
    assert(primeiro_inteiro_maior(A, B));
    assert(primeiro_inteiro_maior_igual(A, B));
    assert(primeiro_inteiro_menor(B, A));
    assert(!primeiro_inteiro_igual(A, B));
    assert(primeiro_inteiro_diferente(A, B));

    liberarMemoriaInteiro(A);
    liberarMemoriaInteiro(B);
}

static void comparacao_modulo_test()
{
    struct Inteiro *A;
    struct Inteiro *B;

    A = criarInteiroComValorString("123", positivo);
    B = criarInteiroComValorString("123", negativo);

    assert(primeiro_inteiro_igual_em_modulo(A, B));

    atualizarNumero(A, "123", positivo);
    atualizarNumero(B, "123", positivo);

    assert(primeiro_inteiro_igual_em_modulo(A, B));

    atualizarNumero(A, "123", negativo);
    atualizarNumero(B, "123", negativo);

    assert(primeiro_inteiro_igual_em_modulo(A, B));

    atualizarNumero(A, "123", negativo);
    atualizarNumero(B, "123", positivo);

    assert(primeiro_inteiro_igual_em_modulo(A, B));
    assert(!primeiro_inteiro_diferente_em_modulo(A,B));

    //

    atualizarNumero(A, "98", positivo);
    atualizarNumero(B, "97", positivo);

    assert(primeiro_inteiro_maior_em_modulo(A, B));
    assert(primeiro_inteiro_maior_igual_em_modulo(A, B));

    atualizarNumero(A, "1", positivo);
    atualizarNumero(B, "0", negativo);

    assert(primeiro_inteiro_maior_em_modulo(A, B));
    assert(primeiro_inteiro_maior_igual_em_modulo(A, B));

    atualizarNumero(A, "23", negativo);
    atualizarNumero(B, "13", positivo);

    assert(primeiro_inteiro_maior_em_modulo(A, B));
    assert(primeiro_inteiro_maior_igual_em_modulo(A, B));

    atualizarNumero(A, "789", negativo);
    atualizarNumero(B, "678", negativo);

    assert(primeiro_inteiro_maior_em_modulo(A, B));
    assert(primeiro_inteiro_maior_igual_em_modulo(A, B));

    //

    atualizarNumero(A, "579", positivo);
    atualizarNumero(B, "4560", positivo);

    assert(primeiro_inteiro_menor_em_modulo(A, B));
    assert(primeiro_inteiro_menor_igual_em_modulo(A,B));

    atualizarNumero(A, "99", positivo);
    atualizarNumero(B, "239312871271323912184713428354548", negativo);

    assert(primeiro_inteiro_menor_em_modulo(A, B));
    assert(primeiro_inteiro_menor_igual_em_modulo(A,B));

    atualizarNumero(A, "123", negativo);
    atualizarNumero(B, "1230", positivo);

    assert(primeiro_inteiro_menor_em_modulo(A, B));
    assert(primeiro_inteiro_menor_igual_em_modulo(A,B));
    assert(primeiro_inteiro_diferente_em_modulo(A,B));

    atualizarNumero(A, "123", negativo);
    atualizarNumero(B, "123", negativo);

    assert(!primeiro_inteiro_menor_em_modulo(A, B));
    assert(primeiro_inteiro_menor_igual_em_modulo(A,B));
    assert(!primeiro_inteiro_diferente_em_modulo(A,B));

    liberarMemoriaInteiro(A);
    liberarMemoriaInteiro(B);
    
}
int main(int argc, char *argv[])
{
    comparacao_test();
    comparacao_modulo_test();
    return 0;
}