#include <assert.h>
#include "../../main/inteiro/inteiro.constructor.h"

static void construirInteiroTest()
{

    struct Inteiro *inteiro = criarInteiroComValorString("0123456789", positivo);

    assert(inteiro != NULL);
    assert(strcmp(inteiro->numero, "0123456789") == 0);
    assert(inteiro->numero[inteiro->size] == '\0');
    assert(inteiro->size == 10);
    assert(inteiro->alloc == 11);
    assert(strlen(inteiro->numero) == inteiro->size);
    assert(inteiro->signal == positivo);
    liberarMemoriaInteiro(inteiro);
}

static void atualizarInteiroTest()
{

    struct Inteiro *inteiro = criarInteiroComValorString("0123456789", positivo);
    assert(inteiro != NULL);
    assert(strcmp(inteiro->numero, "0123456789") == 0);
    assert(inteiro->size == 10);
    assert(inteiro->alloc == 11);
    assert(inteiro->numero[inteiro->size] == '\0');
    assert(strlen(inteiro->numero) == inteiro->size);
    assert(inteiro->signal == positivo);

    atualizarNumero(inteiro, "123", negativo);
    assert(strcmp(inteiro->numero, "123") == 0);
    assert(inteiro->size == 3);
    assert(inteiro->alloc == 11);
    assert(inteiro->numero[inteiro->size] == '\0');
    assert(strlen(inteiro->numero) == inteiro->size);
    assert(inteiro->signal == negativo);

    atualizarNumero(inteiro, "2383428324854745728292985678123219312832197", positivo);
    assert(strcmp(inteiro->numero, "2383428324854745728292985678123219312832197") == 0);
    assert(inteiro->size == 43);
    assert(inteiro->alloc == 44);
    assert(inteiro->numero[inteiro->size] == '\0');
    assert(strlen(inteiro->numero) == inteiro->size);
    assert(inteiro->signal == positivo);

    atualizarNumero(inteiro, "1200", negativo);
    assert(strcmp(inteiro->numero, "1200") == 0);
    assert(inteiro->size == 4);
    assert(inteiro->alloc == 44);
    assert(inteiro->numero[inteiro->size] == '\0');
    assert(strlen(inteiro->numero) == inteiro->size);
    assert(inteiro->signal == negativo);

    liberarMemoriaInteiro(inteiro);
}

static void limpezaInteiroTest()
{

    struct Inteiro *inteiro = criarInteiroComValorString("0123456789", positivo);

    limparInteiro(inteiro);

    assert(strlen(inteiro->numero) == 0);
    assert(inteiro->signal == positivo);
    assert(inteiro->size == 0);

    liberarMemoriaInteiro(inteiro);
}

int main(int argc, char *argv[])
{

    construirInteiroTest();
    atualizarInteiroTest();
    limpezaInteiroTest();

    return 0;
}