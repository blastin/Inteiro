#include <stdio.h>
#include <assert.h>
#include "../../main/string/memory.h"

static void algarismNumericoTest()
{

    assert(ToolsMemory.verificarSeCharEhNumero('0'));
    assert(ToolsMemory.verificarSeCharEhNumero('1'));
    assert(ToolsMemory.verificarSeCharEhNumero('2'));
    assert(ToolsMemory.verificarSeCharEhNumero('3'));
    assert(ToolsMemory.verificarSeCharEhNumero('4'));
    assert(ToolsMemory.verificarSeCharEhNumero('5'));
    assert(ToolsMemory.verificarSeCharEhNumero('6'));
    assert(ToolsMemory.verificarSeCharEhNumero('7'));
    assert(ToolsMemory.verificarSeCharEhNumero('8'));
    assert(ToolsMemory.verificarSeCharEhNumero('9'));
    assert(!ToolsMemory.verificarSeCharEhNumero('A'));
}

static void stringNumericaTest()
{

    const char *string_A = "123456789";
    const char *string_B = "123A456789X";

    assert(ToolsMemory.verificarSeApenasPossuiNumeros(string_A));
    assert(!ToolsMemory.verificarSeApenasPossuiNumeros(string_B));
}

static void inserirAlgarismIntTest()
{

    char *string = NULL;

    assert(ToolsMemory.inserirAlgarismoNumeroInt(&string, 0, 0) == 2);
    assert(ToolsMemory.inserirAlgarismoNumeroInt(&string, 2, 1) == 1);
    assert(ToolsMemory.inserirAlgarismoNumeroInt(&string, 3, 2) == 1);
    assert(ToolsMemory.inserirAlgarismoNumeroInt(&string, 4, 3) == 1);
    assert(ToolsMemory.inserirAlgarismoNumeroInt(&string, 5, 4) == 1);
    assert(ToolsMemory.inserirAlgarismoNumeroInt(&string, 6, 5) == 1);
    assert(ToolsMemory.inserirAlgarismoNumeroInt(&string, 7, 6) == 1);
    assert(ToolsMemory.inserirAlgarismoNumeroInt(&string, 8, 7) == 1);
    assert(ToolsMemory.inserirAlgarismoNumeroInt(&string, 9, 8) == 1);
    assert(ToolsMemory.inserirAlgarismoNumeroInt(&string, 10, 9) == 1);

    assert(string[0] == '0');
    assert(string[1] == '1');
    assert(string[2] == '2');
    assert(string[3] == '3');
    assert(string[4] == '4');
    assert(string[5] == '5');
    assert(string[6] == '6');
    assert(string[7] == '7');
    assert(string[8] == '8');
    assert(string[9] == '9');
    assert(string[10] == '\0');
    assert(strlen(string) == 10);

    assert(ToolsMemory.inserirAlgarismoNumeroInt(&string, 10, 19) == -1);

    free(string);
}

static void inserirAlgarismCharTest()
{
    char *string = NULL;

    assert(ToolsMemory.inserirAlgarismoNumeroChar(&string, 0, '0') == 2);
    assert(ToolsMemory.inserirAlgarismoNumeroChar(&string, 2, '1') == 1);
    assert(ToolsMemory.inserirAlgarismoNumeroChar(&string, 3, '2') == 1);
    assert(ToolsMemory.inserirAlgarismoNumeroChar(&string, 4, '3') == 1);
    assert(ToolsMemory.inserirAlgarismoNumeroChar(&string, 5, '4') == 1);
    assert(ToolsMemory.inserirAlgarismoNumeroChar(&string, 6, '5') == 1);
    assert(ToolsMemory.inserirAlgarismoNumeroChar(&string, 7, '6') == 1);
    assert(ToolsMemory.inserirAlgarismoNumeroChar(&string, 8, '7') == 1);
    assert(ToolsMemory.inserirAlgarismoNumeroChar(&string, 9, '8') == 1);
    assert(ToolsMemory.inserirAlgarismoNumeroChar(&string, 10, '9') == 1);

    assert(string[0] == '0');
    assert(string[1] == '1');
    assert(string[2] == '2');
    assert(string[3] == '3');
    assert(string[4] == '4');
    assert(string[5] == '5');
    assert(string[6] == '6');
    assert(string[7] == '7');
    assert(string[8] == '8');
    assert(string[9] == '9');
    assert(string[10] == '\0');
    assert(strlen(string) == 10);

    assert(ToolsMemory.inserirAlgarismoNumeroInt(&string, 10, 19) == -1);

    free(string);
}

static void limpezaStringTest()
{

    char *string = NULL;

    assert(ToolsMemory.inserirAlgarismoNumeroInt(&string, 0, 0) == 2);
    assert(ToolsMemory.inserirAlgarismoNumeroInt(&string, 2, 1) == 1);
    assert(ToolsMemory.inserirAlgarismoNumeroInt(&string, 3, 2) == 1);
    assert(ToolsMemory.inserirAlgarismoNumeroInt(&string, 4, 3) == 1);
    assert(ToolsMemory.inserirAlgarismoNumeroInt(&string, 5, 4) == 1);
    assert(ToolsMemory.inserirAlgarismoNumeroInt(&string, 6, 5) == 1);
    assert(ToolsMemory.inserirAlgarismoNumeroInt(&string, 7, 6) == 1);
    assert(ToolsMemory.inserirAlgarismoNumeroInt(&string, 8, 7) == 1);
    assert(ToolsMemory.inserirAlgarismoNumeroInt(&string, 9, 8) == 1);
    assert(ToolsMemory.inserirAlgarismoNumeroInt(&string, 10, 9) == 1);

    ToolsMemory.limpeza(string);
    assert(strlen(string) == 0);

    free(string);
}

static void reciclagemMemoriaTest()
{

    char *string = NULL;

    assert(ToolsMemory.inserirAlgarismoNumeroChar(&string, 0, '0') == 2);
    assert(ToolsMemory.inserirAlgarismoNumeroChar(&string, 2, '1') == 1);
    assert(ToolsMemory.inserirAlgarismoNumeroChar(&string, 3, '2') == 1);
    assert(ToolsMemory.inserirAlgarismoNumeroChar(&string, 4, '3') == 1);
    assert(ToolsMemory.inserirAlgarismoNumeroChar(&string, 5, '4') == 1);
    assert(ToolsMemory.inserirAlgarismoNumeroChar(&string, 6, '5') == 1);
    assert(ToolsMemory.inserirAlgarismoNumeroChar(&string, 7, '6') == 1);
    assert(ToolsMemory.inserirAlgarismoNumeroChar(&string, 8, '7') == 1);
    assert(ToolsMemory.inserirAlgarismoNumeroChar(&string, 9, '8') == 1);
    assert(ToolsMemory.inserirAlgarismoNumeroChar(&string, 10, '9') == 1);

    ToolsMemory.limpeza(string);

    // nada deve ser alocado. Deve-se reciclar
    assert(ToolsMemory.inserirAlgarismoNumeroInt(&string, 10, 0) == 0);
    assert(ToolsMemory.inserirAlgarismoNumeroInt(&string, 10, 1) == 0);
    assert(ToolsMemory.inserirAlgarismoNumeroInt(&string, 10, 2) == 0);
    assert(ToolsMemory.inserirAlgarismoNumeroInt(&string, 10, 3) == 0);
    assert(ToolsMemory.inserirAlgarismoNumeroInt(&string, 10, 4) == 0);
    assert(ToolsMemory.inserirAlgarismoNumeroInt(&string, 10, 5) == 0);
    assert(ToolsMemory.inserirAlgarismoNumeroInt(&string, 10, 6) == 0);
    assert(ToolsMemory.inserirAlgarismoNumeroInt(&string, 10, 7) == 0);
    assert(ToolsMemory.inserirAlgarismoNumeroInt(&string, 10, 8) == 0);
    assert(ToolsMemory.inserirAlgarismoNumeroInt(&string, 10, 9) == 1);
    assert(string[10] == '\0');
    assert(strlen(string) == 10);

    free(string);
}

static void obterNumeroTest()
{

    char *string = "12345";

    assert(ToolsMemory.obterNumero(string[0]) == 1);
    assert(ToolsMemory.obterNumero(string[1]) == 2);
    assert(ToolsMemory.obterNumero(string[2]) == 3);
    assert(ToolsMemory.obterNumero(string[3]) == 4);
    assert(ToolsMemory.obterNumero(string[4]) == 5);
}

static void reverterNumeroTest()
{

    char *string = NULL;

    ToolsMemory.inserirAlgarismoNumeroInt(&string, 0, 0);
    ToolsMemory.inserirAlgarismoNumeroInt(&string, 2, 1);
    ToolsMemory.inserirAlgarismoNumeroInt(&string, 3, 2);
    ToolsMemory.inserirAlgarismoNumeroInt(&string, 4, 3);
    ToolsMemory.inserirAlgarismoNumeroInt(&string, 5, 4);
    ToolsMemory.inserirAlgarismoNumeroInt(&string, 6, 5);
    ToolsMemory.inserirAlgarismoNumeroInt(&string, 7, 6);
    ToolsMemory.inserirAlgarismoNumeroInt(&string, 8, 7);
    ToolsMemory.inserirAlgarismoNumeroInt(&string, 9, 8);
    ToolsMemory.inserirAlgarismoNumeroInt(&string, 10, 9);
    
    ToolsMemory.reverter(string);

    assert(strcmp(string, "9876543210") == 0);

    free(string);
}

static void contemAPenasZeroTest(){

    const char * string = "00000000";
    const char * nonzerostring = "13294423423920";


    assert(ToolsMemory.contemApenasZero(string));
    assert(!ToolsMemory.contemApenasZero(nonzerostring));

}

int main(int argc, char *argv[])
{

    algarismNumericoTest();
    stringNumericaTest();
    inserirAlgarismIntTest();
    inserirAlgarismCharTest();
    limpezaStringTest();
    reciclagemMemoriaTest();
    obterNumeroTest();
    reverterNumeroTest();
    contemAPenasZeroTest();

    return 0;
}
