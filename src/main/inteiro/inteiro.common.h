#include <stdlib.h>

#ifndef INTEIRO_COMMON_H
#define INTEIRO_COMMON_H

#define POSITIVO '+'
#define NEGATIVO '-'

enum Signal {

    negativo = -1,
    positivo = 1,

};

struct Inteiro
{
    char *numero;

    size_t alloc;

    size_t size;

    enum Signal signal;
    
    char writeSignal;
};

#endif
