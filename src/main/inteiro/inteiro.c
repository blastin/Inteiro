#include "inteiro.h"

const struct Inteiro_Tools ToolsInteiro = {

    criarInteiroVazio,
    criarInteiroComValorString,

    liberarMemoriaInteiro,
    limparInteiro,
    atualizarNumero,
    alterarSinal,

    primeiro_inteiro_igual,
    primeiro_inteiro_igual_em_modulo,

    primeiro_inteiro_diferente,
    primeiro_inteiro_diferente_em_modulo,

    primeiro_inteiro_maior,
    primeiro_inteiro_maior_igual,
    primeiro_inteiro_maior_em_modulo,
    primeiro_inteiro_maior_igual_em_modulo,

    primeiro_inteiro_menor,
    primeiro_inteiro_menor_igual,
    primeiro_inteiro_menor_em_modulo,
    primeiro_inteiro_menor_igual_em_modulo,

    inteiro_operacao_soma,
    inteiro_operacao_subtracao,
    inteiro_operacao_multiplicao,
    inteiro_operacao_divisao

};