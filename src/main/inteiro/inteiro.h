
#include "inteiro.operacoes.basicas.h"

#ifndef INTEIRO_H
#define INTEIRO_H

struct Inteiro_Tools
{

    struct Inteiro *(*criar)();
    struct Inteiro *(*criarComString)(const char *numero, enum Signal signal);

    void (*liberar)(struct Inteiro *inteiro);
    void (*limpar)(struct Inteiro *inteiro);
    void (*inserirNumero)(struct Inteiro *inteiro, const char *numero, enum Signal signal);
    void (*alterarSinal)(struct Inteiro *inteiro, enum Signal signal);

    bool (*igual_que)(const struct Inteiro *A, const struct Inteiro *B);
    bool (*igual_em_modulo_que)(const struct Inteiro *A, const struct Inteiro *B);
    bool (*diferente_que)(const struct Inteiro *A, const struct Inteiro *B);
    bool (*diferente_em_modulo_que)(const struct Inteiro *A, const struct Inteiro *B);

    bool (*maior_que)(const struct Inteiro *A, const struct Inteiro *B);
    bool (*maior_igual_que)(const struct Inteiro *A, const struct Inteiro *B);
    bool (*maior_em_modulo_que)(const struct Inteiro *A, const struct Inteiro *B);
    bool (*maior_igual_em_modulo_que)(const struct Inteiro *A, const struct Inteiro *B);

    bool (*menor_que)(const struct Inteiro *A, const struct Inteiro *B);
    bool (*menor_igual_que)(const struct Inteiro *A, const struct Inteiro *B);
    bool (*menor_em_modulo_que)(const struct Inteiro *A, const struct Inteiro *B);
    bool (*menor_igual_em_modulo_que)(const struct Inteiro *A, const struct Inteiro *B);

    void (*somar)(struct Inteiro *resultado, struct Inteiro *A, struct Inteiro *B);
    void (*subtrair)(struct Inteiro *resultado, struct Inteiro *A, struct Inteiro *B);
    void (*multiplicar)(struct Inteiro *resultado, struct Inteiro *A, struct Inteiro *B);
    void (*divisao_e_modulo)(struct Inteiro *resultado_quociente, struct Inteiro *resultado_resto, struct Inteiro *A, struct Inteiro *B);
};

extern const struct Inteiro_Tools ToolsInteiro;

#endif
