#include <stdio.h>
#include "inteiro.comparacao.h"

inline static const struct Inteiro *estudo_sinal(const struct Inteiro *A, const struct Inteiro *B)
{
    if (A->signal == positivo)
    {
        return A;
    }
    else
    {
        return B;
    }
}

static const struct Inteiro *maximo_valor(const struct Inteiro *A, const struct Inteiro *B)
{
    if (A && B)
    {
        if (A->size > B->size)
        {
            return estudo_sinal(A, B);
        }
        else if (A->size < B->size)
        {
            return estudo_sinal(B, A);
        }
        else
        {
            int n = strcmp(A->numero, B->numero);

            if (n > 0)
            {
                return estudo_sinal(A, B);
            }
            else if (n < 0)
            {
                return estudo_sinal(B, A);
            }
            else
            {
                if (A->signal != B->signal)
                {
                    return estudo_sinal(A, B);
                }
                else
                {
                    return NULL;
                }
            }
        }
    }
    else
    {
        fprintf(stderr, "[InteiroComparacao::maximo_valor] A ou B é nulo.\n");
        return NULL;
    }
}

static const struct Inteiro *maximo_valor_modulo(const struct Inteiro *A, const struct Inteiro *B)
{
    if (A && B)
    {
        if (A->size > B->size)
        {
            return A;
        }
        else if (A->size < B->size)
        {
            return B;
        }
        else
        {
            int n = strcmp(A->numero, B->numero);

            if (n > 0)
            {
                return A;
            }
            else if (n < 0)
            {
                return B;
            }
            else
            {
                return NULL;
            }
        }
    }
    else
    {
        fprintf(stderr, "[InteiroComparacao::maximo_valor_modulo] A ou B é nulo.\n");
        return NULL;
    }
}

bool primeiro_inteiro_maior(const struct Inteiro *A, const struct Inteiro *B)
{
    return maximo_valor(A, B) == A;
}

bool primeiro_inteiro_maior_igual(const struct Inteiro *A, const struct Inteiro *B)
{
    const struct Inteiro *maior = maximo_valor(A, B);

    return maior == A || maior == NULL;
}

bool primeiro_inteiro_menor(const struct Inteiro *A, const struct Inteiro *B)
{
    return maximo_valor(A, B) == B;
}

bool primeiro_inteiro_menor_igual(const struct Inteiro *A, const struct Inteiro *B)
{
    const struct Inteiro *maior = maximo_valor(A, B);

    return maior == B || maior == NULL;
}

bool primeiro_inteiro_igual(const struct Inteiro *A, const struct Inteiro *B)
{
    return maximo_valor(A, B) == NULL;
}

bool primeiro_inteiro_diferente(const struct Inteiro *A, const struct Inteiro *B)
{
    return maximo_valor(A, B) != NULL;
}

bool primeiro_inteiro_maior_em_modulo(const struct Inteiro *A, const struct Inteiro *B)
{
    return maximo_valor_modulo(A, B) == A;
}
bool primeiro_inteiro_maior_igual_em_modulo(const struct Inteiro *A, const struct Inteiro *B)
{
    const struct Inteiro *maior = maximo_valor_modulo(A, B);

    return maior == A || maior == NULL;
}
bool primeiro_inteiro_menor_em_modulo(const struct Inteiro *A, const struct Inteiro *B)
{
    return maximo_valor_modulo(A, B) == B;
}
bool primeiro_inteiro_menor_igual_em_modulo(const struct Inteiro *A, const struct Inteiro *B)
{
    const struct Inteiro *maior = maximo_valor_modulo(A, B);

    return maior == B || maior == NULL;
}
bool primeiro_inteiro_igual_em_modulo(const struct Inteiro *A, const struct Inteiro *B)
{
    return maximo_valor_modulo(A, B) == NULL;
}
bool primeiro_inteiro_diferente_em_modulo(const struct Inteiro *A, const struct Inteiro *B)
{
    return maximo_valor_modulo(A, B) != NULL;
}