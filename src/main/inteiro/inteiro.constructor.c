#include <stdio.h>
#include "inteiro.constructor.h"

struct Inteiro *criarInteiroVazio()
{
    struct Inteiro *inteiro = (struct Inteiro *)calloc(1, sizeof(struct Inteiro));

    inteiro->signal = positivo;
    inteiro->writeSignal = POSITIVO;

    return inteiro;
}

struct Inteiro *criarInteiroComValorString(const char *numero, enum Signal signal)
{

    struct Inteiro *inteiro = NULL;

    if (ToolsMemory.verificarSeApenasPossuiNumeros(numero))
    {

        inteiro = criarInteiroVazio();

        inteiro->size = strlen(numero);
        inteiro->alloc = inteiro->size + 1;

        inteiro->numero = (char *)malloc((inteiro->size + 1) * sizeof(char));

        strcpy(inteiro->numero, numero);

        inteiro->signal = signal;

        if (signal == negativo)
            inteiro->writeSignal = NEGATIVO;
    }
    else
    {
        fprintf(stderr, "[InteiroConstructor::criarInteiroComValorChar] string(%s) não é númerica\n", numero);
    }

    return inteiro;
}

void liberarMemoriaInteiro(struct Inteiro *inteiro)
{

    if (inteiro)
    {
        free(inteiro->numero);
        free(inteiro);
    }
}

void atualizarNumero(struct Inteiro *inteiro, const char *numero, enum Signal signal)
{

    if (inteiro)
    {
        if (ToolsMemory.verificarSeApenasPossuiNumeros(numero))
        {

            size_t i;
            size_t size = strlen(numero);

            if (!inteiro->numero || strcmp(inteiro->numero, numero) != 0)
            {
                ToolsMemory.limpeza(inteiro->numero);

                for (i = 0; i < size; i++)
                {
                    inteiro->alloc += ToolsMemory.inserirAlgarismoNumeroChar(&inteiro->numero, inteiro->alloc, numero[i]);
                }

                inteiro->size = size;
            }

            alterarSinal(inteiro, signal);
        }
        else
        {
            fprintf(stderr, "[InteiroConstructor::atualizarNumero] string(%s) não é númerica\n", numero);
        }
    }
    else
    {
        fprintf(stderr, "[InteiroConstructor::atualizarNumero] inteiro nulo\n");
    }
}

void alterarSinal(struct Inteiro *inteiro, enum Signal signal)
{
    if (inteiro->signal != signal)
    {
        inteiro->signal = signal;

        if (signal == negativo)
        {
            inteiro->writeSignal = NEGATIVO;
        }
    }
}

extern void limparInteiro(struct Inteiro *inteiro)
{

    if (inteiro)
    {
        ToolsMemory.limpeza(inteiro->numero);
        inteiro->size = 0;
        alterarSinal(inteiro, positivo);
    }
    else
    {
        fprintf(stderr, "[InteiroConstructor::limparInteiro] inteiro nulo\n");
    }
}