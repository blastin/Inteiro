#include <stdio.h>
#include "inteiro.operacoes.basicas.h"

void inteiro_operacao_soma(struct Inteiro *resultado, struct Inteiro *A, struct Inteiro *B)
{
    if (resultado && A && B)
    {
        if (A->numero && B->numero)
        {
            struct Inteiro *maior = NULL;
            struct Inteiro *menor = NULL;

            limparInteiro(resultado);

            if (primeiro_inteiro_maior_igual_em_modulo(A, B))
            {
                if (A->signal == positivo)
                {
                    if (B->signal == positivo)
                    {
                        maior = A;
                        menor = B;
                    }
                    else
                    {
                        alterarSinal(B, positivo);
                        inteiro_operacao_subtracao(resultado, A, B);
                        alterarSinal(B, negativo);

                        return;
                    }
                }
                else
                {
                    if (B->signal == negativo)
                    {
                        maior = A;
                        menor = B;
                        alterarSinal(resultado, negativo);
                    }
                    else
                    {
                        alterarSinal(A, positivo);
                        inteiro_operacao_subtracao(resultado, A, B);
                        alterarSinal(A, negativo);
                        alterarSinal(resultado, negativo);
                        return;
                    }
                }
            }
            else
            {
                if (A->signal == positivo)
                {
                    if (B->signal == positivo)
                    {
                        maior = B;
                        menor = A;
                    }
                    else
                    {
                        alterarSinal(B, positivo);
                        inteiro_operacao_subtracao(resultado, A, B);
                        alterarSinal(B, negativo);
                        return;
                    }
                }
                else
                {
                    if (B->signal == negativo)
                    {
                        maior = B;
                        menor = A;
                        alterarSinal(resultado, negativo);
                    }
                    else
                    {
                        alterarSinal(A, positivo);
                        inteiro_operacao_subtracao(resultado, B, A);
                        alterarSinal(A, negativo);
                        return;
                    }
                }
            }

            // Algoritmo

            long size_menor = menor->size - 1;
            long size_maior = maior->size - 1;

            unsigned short numero_maior;
            unsigned short numero_menor;
            unsigned short soma;
            unsigned short quociente;
            unsigned short resto;

            for (quociente = 0; size_maior >= 0; size_maior--, size_menor--)
            {

                numero_maior = ToolsMemory.obterNumero(maior->numero[size_maior]);

                soma = quociente + numero_maior;

                if (size_menor >= 0)
                {
                    numero_menor = ToolsMemory.obterNumero(menor->numero[size_menor]);
                    soma += numero_menor;
                }

                quociente = soma / 0xAU;
                resto = soma % 0XAU;

                resultado->alloc += ToolsMemory.inserirAlgarismoNumeroInt(&resultado->numero, resultado->alloc, resto);
                resultado->size += 1;
            }

            if (quociente)
            {
                resultado->alloc += ToolsMemory.inserirAlgarismoNumeroInt(&resultado->numero, resultado->alloc, quociente);
                resultado->size += 1;
            }

            if (ToolsMemory.contemApenasZero(resultado->numero))
            {
                atualizarNumero(resultado, "0", resultado->signal);
            }
            else
            {
                while (resultado->numero[resultado->size - 1] == '0')
                {
                    resultado->numero[resultado->size - 1] = '\0';
                    resultado->size--;
                }

                ToolsMemory.reverter(resultado->numero);
            }

            // Algoritmo
        }
        else
        {
            fprintf(stderr, "[InteiroOperacoesBasicas::soma] A->numero ou B->numero é nulo.\n");
        }
    }
    else
    {
        fprintf(stderr, "[InteiroOperacoesBasicas::soma] resultado ou A ou B é nulo.\n");
    }
}

void inteiro_operacao_subtracao(struct Inteiro *resultado, struct Inteiro *A, struct Inteiro *B)
{
    if (resultado && A && B)
    {
        if (A->numero && B->numero)
        {
            struct Inteiro *maior = NULL;
            struct Inteiro *menor = NULL;

            limparInteiro(resultado);

            if (primeiro_inteiro_maior_igual_em_modulo(A, B))
            {

                if (A->signal == positivo)
                {
                    if (B->signal == positivo)
                    {
                        maior = A;
                        menor = B;
                    }
                    else
                    {
                        alterarSinal(B, positivo);
                        inteiro_operacao_soma(resultado, A, B);
                        alterarSinal(B, negativo);
                        return;
                    }
                }
                else
                {

                    if (B->signal == negativo)
                    {
                        maior = A;
                        menor = B;
                        alterarSinal(resultado, negativo);
                    }
                    else
                    {
                        alterarSinal(A, positivo);
                        inteiro_operacao_soma(resultado, A, B);
                        alterarSinal(A, negativo);
                        alterarSinal(resultado, negativo);
                        return;
                    }
                }
            }
            else
            {
                if (A->signal == positivo)
                {
                    if (B->signal == positivo)
                    {
                        maior = B;
                        menor = A;
                        alterarSinal(resultado, negativo);
                    }
                    else
                    {
                        alterarSinal(B, positivo);
                        inteiro_operacao_soma(resultado, A, B);
                        alterarSinal(B, negativo);
                        return;
                    }
                }
                else
                {
                    if (B->signal == negativo)
                    {
                        maior = B;
                        menor = A;
                    }
                    else
                    {
                        alterarSinal(A, positivo);
                        inteiro_operacao_soma(resultado, A, B);
                        alterarSinal(A, negativo);
                        alterarSinal(resultado, negativo);
                        return;
                    }
                }
            }

            // Algoritmo escolar com sistema de propagação linear.

            long size_menor = menor->size - 1;
            long size_maior = maior->size - 1;

            unsigned short numero_maior;
            unsigned short numero_menor;

            short sub;
            short quociente;
            short resto;

            for (quociente = 0; size_maior >= 0; size_maior--, size_menor--)
            {

                numero_maior = ToolsMemory.obterNumero(maior->numero[size_maior]);

                sub = quociente + numero_maior;

                if (size_menor >= 0)
                {

                    if (sub < 0)
                    {
                        sub = 0;
                    }

                    numero_menor = ToolsMemory.obterNumero(menor->numero[size_menor]);
                    sub -= numero_menor;

                    if (sub < 0 || (numero_maior == 0 && numero_menor == 0 && quociente == -1))
                    {
                        sub = 10 + quociente + numero_maior - numero_menor;
                        quociente = -1;
                    }
                    else
                    {
                        quociente = 0;
                    }
                }
                else if (quociente == -1)
                {
                    if (sub >= 0)
                    {
                        quociente = 0;
                    }
                    else
                    {
                        sub = 9;
                    }
                }

                resto = sub;

                resultado->alloc += ToolsMemory.inserirAlgarismoNumeroInt(&resultado->numero, resultado->alloc, resto);
                resultado->size += 1;
            }

            if (ToolsMemory.contemApenasZero(resultado->numero))
            {
                atualizarNumero(resultado, "0", resultado->signal);
            }
            else
            {
                while (resultado->numero[resultado->size - 1] == '0')
                {
                    resultado->numero[resultado->size - 1] = '\0';
                    resultado->size--;
                }
                ToolsMemory.reverter(resultado->numero);
            }

            // Algoritmo escolar com sistema de propagação linear.
        }
        else
        {
            fprintf(stderr, "[InteiroOperacoesBasicas::subtracao] A->numero ou B->numero é nulo.\n");
        }
    }
    else
    {
        fprintf(stderr, "[InteiroOperacoesBasicas::subtracao] resultado ou A ou B é nulo.\n");
    }
}

void inteiro_operacao_multiplicao(struct Inteiro *resultado, struct Inteiro *A, struct Inteiro *B)
{
    if (resultado && A && B)
    {
        if (A->numero && B->numero)
        {
            struct Inteiro *maior = NULL;
            struct Inteiro *menor = NULL;
            struct Inteiro *inteiro = criarInteiroVazio();
            struct Inteiro *auxiliar = criarInteiroVazio();

            size_t size_menor;
            size_t size_maior;

            unsigned short numero_menor;
            unsigned short numero_maior;
            unsigned short multiplicacao;
            unsigned short resto;
            unsigned short quociente;
            unsigned short multiplicador;
            unsigned short i;

            short menor_indice;
            short maior_indice;

            atualizarNumero(resultado, "0", positivo);

            if (primeiro_inteiro_maior_igual_em_modulo(A, B))
            {
                maior = A;
                menor = B;
            }
            else
            {
                maior = B;
                menor = A;
            }

            size_menor = menor->size - 1;
            size_maior = maior->size - 1;

            // Algoritmo Grade escolar

            for (multiplicador = 0, menor_indice = size_menor; menor_indice >= 0; menor_indice--, multiplicador++)
            {
                for (i = 0; i < multiplicador; i++)
                {
                    inteiro->alloc += ToolsMemory.inserirAlgarismoNumeroInt(&inteiro->numero, inteiro->alloc, 0);
                    inteiro->size++;
                }

                for (quociente = 0, maior_indice = size_maior; maior_indice >= 0; maior_indice--)
                {
                    numero_menor = ToolsMemory.obterNumero(menor->numero[menor_indice]);

                    numero_maior = ToolsMemory.obterNumero(maior->numero[maior_indice]);

                    multiplicacao = numero_maior * numero_menor + quociente;

                    quociente = multiplicacao / 0xAU;
                    resto = multiplicacao % 0XAU;

                    inteiro->alloc += ToolsMemory.inserirAlgarismoNumeroInt(&inteiro->numero, inteiro->alloc, resto);
                    inteiro->size++;
                }

                if (!ToolsMemory.contemApenasZero(inteiro->numero) || quociente > 0)
                {

                    if (quociente > 0)
                    {
                        inteiro->alloc += ToolsMemory.inserirAlgarismoNumeroInt(&inteiro->numero, inteiro->alloc, quociente);
                        inteiro->size++;
                    }

                    ToolsMemory.reverter(inteiro->numero);

                    inteiro_operacao_soma(auxiliar, resultado, inteiro);

                    atualizarNumero(resultado, auxiliar->numero, positivo);
                }

                limparInteiro(inteiro);
            }

            liberarMemoriaInteiro(inteiro);
            liberarMemoriaInteiro(auxiliar);

            alterarSinal(resultado, A->signal * B->signal);

            // Algoritmo Grade escolar
        }
        else
        {
            fprintf(stderr, "[InteiroOperacoesBasicas::multiplicacao] A->numero ou B->numero é nulo.\n");
        }
    }
    else
    {
        fprintf(stderr, "[InteiroOperacoesBasicas::multiplicacao] resultado ou A ou B é nulo.\n");
    }
}

void inteiro_operacao_divisao(
    struct Inteiro *resultado_quociente, struct Inteiro *resultado_resto,
    struct Inteiro *A, struct Inteiro *B)
{
    if (resultado_quociente && resultado_resto && A && B)
    {
        if (A->numero && B->numero)
        {
            struct Inteiro *inteiro = criarInteiroVazio();
            struct Inteiro *auxiliar = criarInteiroVazio();

            if (primeiro_inteiro_maior_igual_em_modulo(A, B))
            {
                // algoritmo Grade escolar

                unsigned short quociente;
                unsigned short maior_indice = 0;

                enum Signal signal = B->signal;

                alterarSinal(B, positivo);

                limparInteiro(resultado_quociente);
                limparInteiro(resultado_resto);

                do
                {

                    inteiro->alloc += ToolsMemory.inserirAlgarismoNumeroChar(&inteiro->numero, inteiro->alloc, A->numero[maior_indice++]);
                    inteiro->size += 1;

                    quociente = 0;

                    atualizarNumero(auxiliar, inteiro->numero, positivo);

                    while (primeiro_inteiro_maior_igual(auxiliar, B))
                    {
                        quociente += 1;
                        inteiro_operacao_subtracao(inteiro, auxiliar, B);
                        atualizarNumero(auxiliar, inteiro->numero, positivo);
                    }

                    resultado_quociente->alloc += ToolsMemory.inserirAlgarismoNumeroInt(&resultado_quociente->numero, resultado_quociente->alloc, quociente);
                    resultado_quociente->size += 1;

                    if (auxiliar->numero[0] == '0')
                    {
                        limparInteiro(inteiro);
                    }
                    else
                    {
                        atualizarNumero(inteiro, auxiliar->numero, positivo);
                    }
                } while (A->numero[maior_indice]);

                atualizarNumero(resultado_resto, auxiliar->numero, positivo);

                if (resultado_quociente->numero[0] == '0')
                {

                    ToolsMemory.reverter(resultado_quociente->numero);
                    while (resultado_quociente->numero[resultado_quociente->size - 1] == '0')
                    {
                        resultado_quociente->numero[resultado_quociente->size - 1] = '\0';
                        resultado_quociente->size--;
                    }
                    ToolsMemory.reverter(resultado_quociente->numero);
                }

                alterarSinal(B, signal);

                alterarSinal(resultado_quociente, A->signal * B->signal);

                if (A->signal == signal)
                {
                    alterarSinal(resultado_resto, A->signal);
                }
                else
                {
                    atualizarNumero(auxiliar, "1", positivo);

                    inteiro_operacao_subtracao(inteiro, resultado_quociente, auxiliar);
                    atualizarNumero(resultado_quociente, inteiro->numero, negativo);

                    inteiro_operacao_multiplicao(auxiliar, B, resultado_quociente);
                    inteiro_operacao_subtracao(resultado_resto, A, auxiliar);
                }

                // algoritmo Grade escolar
            }
            else
            {
                if (A->signal == B->signal)
                {
                    atualizarNumero(resultado_quociente, "0", positivo);
                    atualizarNumero(resultado_resto, A->numero, A->signal);
                }
                else
                {
                    atualizarNumero(resultado_quociente, "1", negativo);
                    inteiro_operacao_multiplicao(auxiliar, B, resultado_quociente);
                    inteiro_operacao_subtracao(resultado_resto, A, auxiliar);
                }
            }

            liberarMemoriaInteiro(auxiliar);
            liberarMemoriaInteiro(inteiro);
        }
        else
        {
            fprintf(stderr, "[InteiroOperacoesBasicas::divisao] A->numero ou B->numero é nulo.\n");
        }
    }
    else
    {
        fprintf(stderr, "[InteiroOperacoesBasicas::divisao] resultado_quociente ou resultado_resto ou A ou B é nulo.\n");
    }
}
