#include "inteiro.common.h"
#include "../string/memory.h"

#ifndef INTEIRO_COMPARATIONS_H
#define INTEIRO_COMPARATIONS_H

extern bool primeiro_inteiro_maior(const struct Inteiro *A, const struct Inteiro *B);
extern bool primeiro_inteiro_maior_igual(const struct Inteiro *A, const struct Inteiro *B);
extern bool primeiro_inteiro_menor(const struct Inteiro *A, const struct Inteiro *B);
extern bool primeiro_inteiro_menor_igual(const struct Inteiro *A, const struct Inteiro *B);
extern bool primeiro_inteiro_igual(const struct Inteiro *A, const struct Inteiro *B);
extern bool primeiro_inteiro_diferente(const struct Inteiro *A, const struct Inteiro *B);
extern bool primeiro_inteiro_maior_em_modulo(const struct Inteiro *A, const struct Inteiro *B);
extern bool primeiro_inteiro_maior_igual_em_modulo(const struct Inteiro *A, const struct Inteiro *B);
extern bool primeiro_inteiro_menor_em_modulo(const struct Inteiro *A, const struct Inteiro *B);
extern bool primeiro_inteiro_menor_igual_em_modulo(const struct Inteiro *A, const struct Inteiro *B);
extern bool primeiro_inteiro_igual_em_modulo(const struct Inteiro *A, const struct Inteiro *B);
extern bool primeiro_inteiro_diferente_em_modulo(const struct Inteiro *A, const struct Inteiro *B);

#endif
