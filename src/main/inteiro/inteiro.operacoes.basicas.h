#include "inteiro.constructor.h"
#include "inteiro.comparacao.h"

#ifndef INTEIRO_OPERACOES_BASICAS_H
#define INTEIRO_OPERACOES_BASICAS_H

extern void inteiro_operacao_soma(struct Inteiro *resultado, struct Inteiro *A, struct Inteiro *B);
extern void inteiro_operacao_subtracao(struct Inteiro *resultado, struct Inteiro *A, struct Inteiro *B);
extern void inteiro_operacao_multiplicao(struct Inteiro *resultado, struct Inteiro *A, struct Inteiro *B);
extern void inteiro_operacao_divisao(struct Inteiro *resultado_quociente, struct Inteiro *resultado_resto, struct Inteiro *A, struct Inteiro *B);

#endif
