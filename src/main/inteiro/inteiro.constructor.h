#include "inteiro.common.h"
#include "../string/memory.h"

#ifndef INTEIRO_FUNCTIONS_H
#define INTEIRO_FUNCTIONS_H

extern struct Inteiro *criarInteiroVazio();
extern struct Inteiro *criarInteiroComValorString(const char *numero, enum Signal signal);
extern void liberarMemoriaInteiro(struct Inteiro *inteiro);
extern void atualizarNumero(struct Inteiro *inteiro, const char *numero, enum Signal signal);
extern void alterarSinal(struct Inteiro * inteiro, enum Signal signal);
extern void limparInteiro(struct Inteiro * inteiro);
#endif