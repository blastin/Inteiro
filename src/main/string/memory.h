#include <stdlib.h>
#include <string.h>

#ifndef MEMOMORY_INTEIRO_HEADER
#define MEMOMORY_INTEIRO_HEADER

#ifndef bool
typedef enum {
    false,
    true
} bool;
#endif

extern size_t inserirAlgarismNumericoInt(char **string, size_t alloc, unsigned short algarism);
extern size_t inserirAlgarismNumericoChar(char **string, size_t alloc, char algarism);
extern bool stringNumerica(const char *string);
extern bool algarismoNumerico(char algarism);
extern void limparString(char *string);
extern unsigned short obterNumero(const char algarismo);
extern void reverterString(char *string);
extern bool stringNumericaAPenasContemZero(const char *string);

struct Memory
{

    size_t (*inserirAlgarismoNumeroInt)(char **string, size_t alloc, unsigned short algarism);
    size_t (*inserirAlgarismoNumeroChar)(char **string, size_t alloc, char algarism);
    bool (*verificarSeApenasPossuiNumeros)(const char *string);
    bool (*verificarSeCharEhNumero)(char algarism);
    void (*limpeza)(char *string);
    unsigned short (*obterNumero)(const char algarismo);
    void (*reverter)(char *string);
    bool (*contemApenasZero)(const char *string);
};

extern const struct Memory ToolsMemory;

#endif
