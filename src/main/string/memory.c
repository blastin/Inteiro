#include <stdio.h>
#include "memory.h"

const struct Memory ToolsMemory = {
    inserirAlgarismNumericoInt,
    inserirAlgarismNumericoChar,
    stringNumerica,
    algarismoNumerico,
    limparString,
    obterNumero,
    reverterString,
    stringNumericaAPenasContemZero};

size_t inserirAlgarismNumericoChar(char **string, size_t alloc, char algarism)
{
    return inserirAlgarismNumericoInt(string, alloc, (unsigned short)(algarism - 0x30));
}

size_t inserirAlgarismNumericoInt(char **string, size_t alloc, unsigned short algarismo)
{
    if (string)
    {
        if (algarismoNumerico((char)(algarismo + 0x30)))
        {
            size_t size = 1;

            if (!*string)
            {
                (*string) = (char *)calloc(size + 1, sizeof(char));

                alloc = 2;
            }
            else
            {
                size += strlen((*string));

                if (size == alloc)
                {
                    (*string) = (char *)realloc((*string), (size + 1) * sizeof(char));

                    alloc = 1;
                }
                else
                {
                    alloc = 0;
                }
            }

            (*string)[size - 1] = (char)(algarismo + 0x30);
            (*string)[size] = '\0';

            return alloc;
        }
        else
        {
            fprintf(stderr, "[Memory::inserir] Caracter(%d) não é número ... \n", algarismo);
        }
    }
    else
    {
        fprintf(stderr, "[Memory::inserir] String = NULL ... \n");
    }

    return -1;
}

bool stringNumerica(const char *string)
{

    while (*string)
    {
        if (!algarismoNumerico(*string++))
            return false;
    }

    return true;
}

bool algarismoNumerico(char algarism)
{
    if (algarism >= 0x30 && algarism <= 0x39)
        return true;
    else
        return false;
}

void limparString(char *string)
{
    if (string)
    {
        size_t size = strlen(string);
        memset(string, '\0', size);
    }
}

unsigned short obterNumero(const char algarismo)
{

    if (algarismoNumerico(algarismo))
    {
        return (unsigned short)algarismo - 0x30;
    }
    else
    {
        fprintf(stderr, "[Memory::obterNumero] Caracter(%d) não é número ... \n", algarismo);
    }

    return -1;
}

void reverterString(char *string)
{
    if (string)
    {
        const size_t size = strlen(string) - 1;
        const size_t length = size / 2;

        register size_t i;

        char chr;

        for (i = 0; i <= length; i++)
        {
            chr = string[i];

            string[i] = string[size - i];

            string[size - i] = chr;
        }
    }
    else
    {
        fprintf(stderr, "[Memory::reverterString] String = NULL ... \n");
    }
}

bool stringNumericaAPenasContemZero(const char *string)
{
    while (*string)
    {
        if (*string++ != '0')
        {
            return false;
        }
    }

    return true;
}